import React, { useEffect, useState, Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";

//css
import "./assets/css/plugins.css";
import "./assets/css/bootstrap.min.css";
import "./assets/css/style.css";
import "./assets/css/responsive.css";

//components
import Homepage from "./components/Homepage/Homepage";
import Productdetails from "./components/Productdetails/Productdetails";
import Notfound from "./components/Notfoundpage/Notfoundpage";
import UserProfile from "./components/UserProfile/UserProfile";

function App() {
	return (
		<main>
			<BrowserRouter>
				<Switch>
					<Route path="/" component={Homepage} exact />
					<Route path="/product/:id" component={Productdetails} />
					{/* <Route path="" component={Notfound} /> */}
					<Route path="/user/:id" component={UserProfile} />
				</Switch>
			</BrowserRouter>
		</main>
	);
}

export default App;
