export const SITEURL = 'https://bajarsongi.visionssoftwaresolution.com/';
export const CURRENCY = '₹';
export function getDifferenceInDays(date1, date2) {
    const diffInMs = Math.abs(date2 - date1);
    return diffInMs / (1000 * 60 * 60 * 24);
}
  