import React, {useEffect , useState , Suspense} from 'react';
import { Link } from 'react-router-dom';
import * as Helper from '../../Helper';

//css
import  '../../assets/css/notfoundstyle.css';

import axios from 'axios';

export default function Notfoundpage() {

  const [Sitesettings , setSitesettings] = useState([]);
  let logo = '';

  const notfoundstyle = {
    'width' : "30%",
  };

  //API call
  let url = Helper.SITEURL+'admin/api/settings';
  const Apicall = () => {
    axios
        .get(url)
        .then((res) => {
            if (res.status === 200) {
              if(res.data.data !== undefined) {
                setSitesettings(res.data.data);
              }
              
            }else{
              setSitesettings([]);
            }
        })
        .catch((err) => {
            setSitesettings([]);
        });
  };

  useEffect(() => {
      Apicall();
  }, []);

  return(
    <div id="notfound">
        <div class="notfound">
          <div class="notfound-404">
            <img src={Helper.SITEURL+'admin/'+Sitesettings.notfound_logo} alt="404" style={notfoundstyle}/>
          </div>
          <h2>Oops! Nothing was found</h2>
          <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable. <Link to="/">Return to homepage</Link></p>
          <div class="notfound-social">
            <Link to="/facebook"><i class="fa fa-facebook"></i></Link>
            <Link to="/twitter"><i class="fa fa-twitter"></i></Link>
            <Link to="/google-plus "><i class="fa fa-google-plus"></i></Link>
          </div>
        </div>
    </div>
  );
}
