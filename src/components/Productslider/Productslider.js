import React from 'react';
import Slider from "react-slick";

// Import css files
import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

import Product from '../Product/Product';


export default function Productslider(props){

  var settings = {
    autoplay: false,
    autoplaySpeed: 4000,
    dots: false,
    infinite: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    fade: false
    
  };

  return(
    <>
      <div className="section">
        <div className="container">
            <div className="row">
              <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                  <div className="section-header text-center">
                    <h2 className="h2">{props.slidetitle}</h2>
                    <p>{props.slidedescription}</p>
                  </div>
                  <div className="productSlider grid-products slick-initialized slick-slider">
                        <Slider {...settings}>

                            { props.product !== undefined &&  props.product.map((sdata, i) => { 
                                   return (< Product col='12' product= {sdata} settings= {props.settings} type='type2'/>)
                            })}

                        </Slider>
                  </div>
              </div>
            </div>
        </div>
      </div>
    </>
  );
}



        