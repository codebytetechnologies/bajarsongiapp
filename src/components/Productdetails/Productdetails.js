import React, {useEffect , useState , Suspense} from 'react';

import Topbanner from '../Topbanner/Topbanner';
import Productimageslider from '../Productimageslider/Productimageslider';
import Breadcrumb from '../Breadcrumb/Breadcrumb';
import Productprice from '../Productprice/Productprice';
import Productreview from '../Productreview/Productreview';
import Deliverybanner from '../Deliverybanner/Deliverybanner';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import * as Helper from '../../Helper';
import Loader from '../Loader/Loader';
import { useParams } from "react-router-dom";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Productslider from '../Productslider/Productslider'


import axios from 'axios';

import '../../assets/css/overwrite.css';


export default function Productdetails(props){

    const params = useParams();

    const [loaderState, setLoaderState] = useState(true);
    const [Sitesettings , setSitesettings] = useState([]);

    const successstyle = {
        'margin' : "2%",
    };


    //API call
      let url = Helper.SITEURL+'admin/api/settings';
      const Apicall = () => {
        axios
            .get(url)
            .then((res) => {
                if (res.status === 200) {

                  setLoaderState(false);
                  if(res.data.data !== undefined) {
                    setSitesettings(res.data.data);
                  }
                
                }else{
                  setLoaderState(true);
                  setSitesettings([]);
                }
            })
            .catch((err) => {
                setLoaderState(true);
                setSitesettings([]);
            });
      };

      useEffect(() => {
          Apicall();
      }, [setSitesettings]);
    
    //Product Details
    const [Product , setProduct] = useState([]);
    useEffect(() => {
        productCall(params.id);
    }, [params.id]);

    //Similler Products
    // const [Similerproduct , setSimilerproduct] = useState([]);
    // let similerproducturl = Helper.SITEURL+'admin/api/similar-products/3';
    // const Apisimilerproductcall = () => {
    //     axios
    //         .get(similerproducturl)
    //         .then((res) => {

    //             if (res.status === 200) {
    
    //                 setLoaderState(false);
    //                 if(res.data.data !== undefined) {
    //                     setSimilerproduct(res.data.data);
    //                 }
                    
    //             }else{
    //                 setLoaderState(true);
    //                 setSimilerproduct([]);
    //             }
    //         })
    //         .catch((err) => {
    //             setLoaderState(true);
    //             setSimilerproduct([]);
    //         });
    //     };
    // useEffect(() => {
    //     Apisimilerproductcall();
    // }, []);


    function productCall(slug){


        let producturl = Helper.SITEURL+'admin/api/product/'+slug;
        axios
            .get(producturl)
            .then((res) => {

                if (res.status === 200) {
                    setLoaderState(false);
                    if(res.data.data[0] !== undefined) {
                    setProduct(res.data.data[0]);
                    }
                    
                }else{
                    setLoaderState(true);
                    setProduct([]);
                }
            })
            .catch((err) => {
                setLoaderState(true);
                setProduct([]);
            });

    }

  console.log(Product);

  return(
    <>

          {loaderState !== false  ? <Loader />  : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Topbanner settings = {Sitesettings} />  : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Header settings = {Sitesettings} /> : ''}
          
          
   

        <div id="page-content ">
        <div id="MainContent" className="main-content" role="main">

            <Breadcrumb />


            <div id="ProductSection-product-template" className="product-template__container prstyle1 container">

                <div className="product-single">
                    <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                            {Product !== undefined ? (<Productimageslider product= {Product} settings = {Sitesettings} />) : ''}
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div className="product-single__meta">
                                <h1 className="product-single__title">{Product.product_name}</h1>

                                <div className="prInfoRow">
                                    <div className="product-stock"> 

                                        {Product.product_stock_quantity !== undefined && Product.product_stock_quantity > 0 ? (
                                            <span className="instock ">In Stock</span> 
                                        ) : (
                                            <span className="outstock hide">Unavailable</span> 
                                        )}
                                    
                                    </div>
                                </div>
                                <div className="prInfoRow">

                                    <div className="product-review"><a className="reviewLink" href="#tab2"><i
                                                className="font-13 fa fa-star"></i><i className="font-13 fa fa-star"></i><i
                                                className="font-13 fa fa-star"></i><i
                                                className="font-13 fa fa-star-o"></i><i
                                                className="font-13 fa fa-star-o"></i><span className="spr-badge-caption">6
                                                reviews</span></a></div>
                                </div>

                                {Product.product_retail_price !== undefined && Product.product_retail_price > 0 ? (<Productprice price ={Product.product_retail_price} sale ={Product.product_offer_percentage} offerstatus={Product.product_offer_status} />) : ''}
                                
                                <div className="orderMsg" data-user="23" data-time="24">
                                    <img src="/assets/images/order-icon.jpg" alt="" /> <strong
                                        className="items">5</strong> sold in last <strong className="time">26</strong> hours
                                </div>
                            </div>

                            <div id="quantity_message">Hurry! Only <span className="items">4</span> left in stock.</div>
                            <form method="post" action="http://annimexweb.com/cart/add"
                                id="product_form_10508262282" accept-charset="UTF-8"
                                className="product-form product-form-product-template hidedropdown"
                                enctype="multipart/form-data">
                                

                                <div className="swatch clearfix swatch-0 option1" data-option-index="0">
                                    <div className="product-form__item">


                                    { Product.options !== undefined && Product.options !== "" && Product.options.map((option, i) => { 

                                                let switchid = 'swatch-'+option.quantity+'-'+option.varient;
                                                let name = option.quantity+' '+option.varient;
                                                
                                                return (

                                                    <>

                                                    {option.stock_quantity !== undefined && option.stock_quantity > 0 ? (
                                                        <div data-value={name} className="swatch-element color red available">
                                                            <input className="swatchInput" id={switchid}  value= {name} onClick={() => { productCall(option.product_slug); }}/>
                                                            <label className="swatchLbl color medium rectangle" for={switchid}
                                                                style={{ backgroundImage: `url(${option.product_image})` }}
                                                                title={name}></label>
                                                            <span className="slVariant">{option.quantity} {option.varient}</span>
                                                        </div>
                                                    ) : (
                                                        <div data-value={name}
                                                            className="swatch-element color orange available notAvailable">
                                                            <input className="swatchInput" id={switchid}  value={name} onClick={() => { productCall(option.product_slug); }}/>
                                                            <label className="swatchLbl color medium rectangle" for={switchid}
                                                                style={{ backgroundImage: `url(${option.product_image})` }}
                                                                title={name}></label>
                                                            <span className="slVariant">{option.quantity} {option.varient}</span>
                                                        </div>
                                                    )}

                                                    </>   
                                                            
                                                    
                                                )

                                    })}
                                        
                                    </div>
                                </div>

                                <div className="product-form__item--quantity">
                                    <div className="wrapQtyBtn">
                                        <div className="qtyField">
                                            <a className="qtyBtn minus" href="javascript:void(0);"><i
                                                    className="fa anm anm-minus-r" aria-hidden="true"></i></a>
                                            <input type="text" id="Quantity" name="quantity" value="1"
                                                className="product-form__input qty" />
                                            <a className="qtyBtn plus" href="javascript:void(0);"><i
                                                    className="fa anm anm-plus-r" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="product-action clearfix">

                                    <div className="product-form__item--submit">
                                        <button type="button" name="add" className="btn product-form__cart-submit">
                                            <span>Add to cart</span>
                                        </button>
                                    </div>
                                    <div className="shopify-payment-button" data-shopify="payment-button">
                                        <button type="button"
                                            className="shopify-payment-button__button shopify-payment-button__button--unbranded">Buy
                                            it now</button>
                                    </div>
                                </div>
                            </form>
                            <div className="display-table shareRow">
                                <div className="display-table-cell medium-up--one-third">
                                <div className="social-sharing">
                                        <a target="_blank" href="#"
                                            className="btn btn--small btn--secondary btn--share share-facebook"
                                            title="Share on Facebook">
                                            <i className="fa fa-facebook-square" aria-hidden="true"></i> <span
                                                className="share-title" aria-hidden="true">Share</span>
                                        </a>
                                        <a target="_blank" href="#"
                                            className="btn btn--small btn--secondary btn--share share-twitter"
                                            title="Tweet on Twitter">
                                            <i className="fa fa-twitter" aria-hidden="true"></i> <span
                                                className="share-title" aria-hidden="true">Tweet</span>
                                        </a>
                                        <a href="#" title="Share on google+"
                                            className="btn btn--small btn--secondary btn--share">
                                            <i className="fa fa-google-plus" aria-hidden="true"></i> <span
                                                className="share-title" aria-hidden="true">Google+</span>
                                        </a>
                                        <a target="_blank" href="#"
                                            className="btn btn--small btn--secondary btn--share share-pinterest"
                                            title="Pin on Pinterest">
                                            <i className="fa fa-pinterest" aria-hidden="true"></i> <span
                                                className="share-title" aria-hidden="true">Pin it</span>
                                        </a>
                                        <a href="#" className="btn btn--small btn--secondary btn--share share-pinterest"
                                            title="Share by Email" target="_blank">
                                            <i className="fa fa-envelope" aria-hidden="true"></i> <span
                                                className="share-title" aria-hidden="true">Email</span>
                                        </a>
                                    </div>
                                </div>
                               
                            </div>

                            <p id="freeShipMsg" className="freeShipMsg" data-price="199"><i className="fa fa-truck"
                                    aria-hidden="true"></i> GETTING CLOSER! ONLY <b className="freeShip"><span
                                        className="money" data-currency-usd="$199.00"
                                        data-currency="USD">$199.00</span></b> AWAY FROM <b>FREE SHIPPING!</b></p>
                            <p className="shippingMsg"><i className="fa fa-clock-o" aria-hidden="true"></i> ESTIMATED
                                DELIVERY BETWEEN <b id="fromDate">Wed. May 1</b> and <b id="toDate">Tue. May 7</b>.
                            </p>
                            <div className="userViewMsg" data-user="20" data-time="11000"><i className="fa fa-users"
                                    aria-hidden="true"></i> <strong className="uersView">14</strong> PEOPLE ARE LOOKING
                                FOR THIS PRODUCT</div>
                        </div>
                    </div>
                </div>

                <Deliverybanner/>

                <Tabs className ="tabs-listing">
                    <TabList className ="product-tabs">
                        <Tab className =""><a className ="tablink">Product Details</a></Tab>
                        <Tab className =""><a className ="tablink">Product Reviews</a></Tab>
                        <Tab className =""><a className ="tablink">Shipping &amp; Returns</a></Tab>
                    </TabList>

                    <TabPanel>
                        <p style={successstyle}  dangerouslySetInnerHTML={{ __html: Product.product_description}}></p>
                    </TabPanel>
                    <TabPanel>
                        <p style={successstyle} >
                           Reviw tab
                        </p>
                    </TabPanel>
                    <TabPanel>
                        <p style={successstyle}  dangerouslySetInnerHTML={{ __html: Sitesettings.return_policy}}></p>
                    </TabPanel>
                </Tabs>
                
                

                {/* <Productslider slidetitle='Similar Products' slidedescription='' product ={Similerproduct} settings ={Sitesettings} /> */}
                
            </div>

        </div>

        </div>

    {loaderState === false && Sitesettings !== undefined  ? <Footer settings = {Sitesettings} type = 'productdetilas'/> : ''}
 
    </>
  );

}
