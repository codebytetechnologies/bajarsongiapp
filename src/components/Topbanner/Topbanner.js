import React , { useState, useCallback , useEffect} from 'react';
import wplogo from '../../assets/images/whatsapp.png';
import { Modal ,Button, Form  } from "react-bootstrap";

import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import PhoneInput from 'react-phone-number-input';
import * as Helper from '../../Helper';


//CSS
import 'react-phone-number-input/style.css';

//Axios
import axios from 'axios';

export default function Topbanner(props) {

    const [value, setValue] = useState();

    // form validation rules 
    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Please Enter Your Name.'),
        contact: Yup.string()
            .required('Please Enter Your Valid Contact Number.'),
        email: Yup.string()
            .required('Please Enter Your Email.')
            .email('Invalid Email.'),   
        password: Yup.string()
            .required('Please Enter your Password.'),
            // .matches(
            //     /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
            //     "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
            // ),
        confirmpassword: Yup.string()
            .required('Please Enter your Confirm Password.')
            .oneOf([Yup.ref("password"), null], "Passwords must match")
    
    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    //get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;
    const [showSuccess, setShowSuccess] = useState(false);
    const [showError, setShowError] =  useState(false);
    const [message, setMessage] =  useState('');


    const marginbottom = {
        'margin-bottom' : "-106px"
    };
    
    const successstyle = {
        'text-align' : "center",
        'margin-left' : "31%",
        'float': 'left'
    };


    //API call
    function onSubmit(data) {

        if(data.email!== undefined && data.email !== ''){

          const formData = new FormData();
        
          formData.append("firstname", data.name);
          formData.append("email", data.email);
          formData.append("contact", data.contact);
          formData.append("password", data.password);
          formData.append("confirm_password", data.confirmpassword);
          
            axios
            .post(Helper.SITEURL +'/admin/api/customer-registration/',formData , {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            })
            .then((res) => {
                console.log(res);
                if (res.data.success === true) {
                  setShowSuccess(true);
                  reset();
                  setMessage(res.data.message);
                }else{
                  setShowError(true);
                  setMessage(res.data.message);
                }
            })
            .catch((err) => {
                console.log(err);
            });

        }
        return false;
    }

    useEffect(() => {
      const interval = setInterval(() => {
        setShowSuccess(false);
        setShowError(false);
      }, 10000);
      return () => clearInterval(interval);
    }, []);


    //Login
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleHide = () => setShow(false);

    //signup
    const [sshow, setSshow] = useState(false);
    const handlesShow = () => setSshow(true);
    const handlesHide = () => setSshow(false);

  return(
    <>
      <div className="top-header">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-10 col-sm-8 col-md-5 col-lg-4">

                        <p className="phone-no"><img src={wplogo} style={{ width: "21px"}} /> {props.settings.sale_contact}</p>
                    </div>

                    <div className="col-2 col-sm-4 col-md-3 col-lg-4 text-right ml-auto">
                        <span className="user-menu d-block d-lg-none"><i className="anm anm-user-al"
                                aria-hidden="true"></i></span>
                        <ul className="customer-links list-inline">
                            <li><a href="javascript:void(0)" onClick={handleShow} >Login</a></li>
                            <li><a href="javascript:void(0)" onClick={handlesShow} >SignUp</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <Modal show={show}>
            <>
                
                    <div className="modal-header border-bottom-0">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={handleHide}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="form-title text-center">
                            <h4>Login</h4>
                        </div>
                        <div className="d-flex flex-column text-center">
                            <form>
                                                { showSuccess ?
                                                <div class="row">
                                                    <div id="success" class="alert alert-success" style = {successstyle}>
                                                        <strong>Success!</strong> {message}.
                                                    </div>
                                                </div>
                                                : '' }
                                                { showError ?
                                                <div class="row">
                                                    <div id="error" class="alert alert-danger" style = {successstyle}>
                                                        <strong>Danger!</strong> {message}.
                                                    </div>
                                                </div>
                                                : '' }
                            <div className="form-group">
                                
                                <input type="email" className="form-control" id="email" placeholder="Email" />

                            </div>
                            <div className="form-group">

                                <input type="password" className="form-control" id="password" placeholder="Password" />
                            </div>
                            <button type="button" className="btn btn-info btn-block btn-round">
                            Login
                            </button>
                            </form>
                            <div className="text-center text-muted delimiter">or use a social network</div>
                            <div className="d-flex justify-content-center social-buttons">
                            <button
                                type="button"
                                className="btn btn-secondary btn-round"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Facebook"
                                >
                            <i className="fab fa-facebook"></i>
                            </button>
                            <button
                                type="button"
                                className="btn btn-secondary btn-round"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Google"
                                >
                            <i class="fab fa-google"></i>
                            </button>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <div class="signup-section">Not a member yet? <a href="#a" class="text-info"> Sign Up</a>.</div>
                    </div>
                
            </>
            
        </Modal>

        <Modal show={sshow}>
            <>
                
                    <div className="modal-header border-bottom-0">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={handlesHide}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="form-title text-center">
                            <h4>Sign Up</h4>
                        </div>
                        <div className="d-flex flex-column text-center">
                                    <form onSubmit={handleSubmit(onSubmit)}>
                                                { showSuccess ?
                                                <div class="row">
                                                    <div id="success" class="alert alert-success" style = {successstyle}>
                                                        <strong>Success!</strong> {message}.
                                                    </div>
                                                </div>
                                                : '' }
                                                { showError ?
                                                <div class="row">
                                                    <div id="error" class="alert alert-danger" style = {successstyle}>
                                                        <strong>Danger!</strong> {message}.
                                                    </div>
                                                </div>
                                                : '' }    
                                                <div className="form-group">
                                                    <input {...register('email' , { required: true })} className={`form-control ${errors.email ? 'invalid' : ''}`} type="email" name="email" placeholder="Email" />
                                                    <div className="invalid-feedback">{errors.email?.message}</div>
                                                </div>
                                                <div className="form-group">
                                                    <PhoneInput {...register('contact' , { required: true, min: 10 })}  name="contact" international defaultCountry="IN" value={value} onChange={setValue} />
                                                    <div className="invalid-feedback">{errors.contact?.message}</div>
                                                </div>
                                                <div className="form-group">
                                                    <input {...register('name' , { required: true })} className={`form-control ${errors.name ? 'invalid' : ''}`} type="text" name="name" placeholder="Name" />
                                                    <div className="invalid-feedback">{errors.name?.message}</div>
                                                </div>
                                                <div className="form-group">
                                                    <input {...register('password' , { required: true })} className={`form-control ${errors.password ? 'invalid' : ''}`} type="password" name="password" placeholder="Password" />
                                                    <div className="invalid-feedback">{errors.password?.message}</div>
                                                </div>
                                                <div className="form-group">
                                                <input {...register('confirmpassword' , { required: true })} className={`form-control ${errors.confirmpassword ? 'invalid' : ''}`} type="password" name="confirmpassword" placeholder="Confirm Password" />
                                                    <div className="invalid-feedback">{errors.confirmpassword?.message}</div>
                                                </div>

                                                <input className="btn btn-info btn-block btn-round" type="submit" value="Submit" />
                                                
                                    </form>
                            <div className="text-center text-muted delimiter">Sign up with</div>
                            <div className="d-flex justify-content-center social-buttons">
                            <button
                                type="button"
                                className="btn btn-secondary btn-round"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Facebook"
                                >
                            <i className="fab fa-facebook"></i>
                            </button>
                            <button
                                type="button"
                                className="btn btn-secondary btn-round"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Google"
                                >
                            <i class="fab fa-google"></i>
                            </button>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer d-flex justify-content-center">
                        <div class="signup-section">Exiesting member Please <a href="#a" class="text-info"> Login In</a></div>
                    </div>
                
            </>
            
        </Modal>
        
    </>
  );

}
