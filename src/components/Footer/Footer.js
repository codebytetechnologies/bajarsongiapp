import React from 'react';
import Delivery from '../Delivery/Delivery';
import Deliverybanner from '../Deliverybanner/Deliverybanner';

//logo
import paylogo from '../../assets/images/safepayment.png';
import * as Helper from '../../Helper';

export default function Footer(props){

  let logo = Helper.SITEURL+'admin/'+props.settings.site_logo;

  return(   <> 
                    <footer id="footer" class="footer-2">

                    {props.type !== undefined  &&  props.type === 'productdetilas' ? '' : < Delivery />}
                    
                        
                        <div className="site-footer">
                            <div className="container">
                                <div className="footer-top">
                                    <div className="row">
                                        <div className="col-12 col-sm-12 col-md-3 col-lg-3 contact-box">
                                            <div className="footer-logo mb-3"><img src={logo} alt={logo} title={props.settings.Title} />
                                            </div>
                                            <ul className="addressFooter">
                                                <li><i className="icon anm anm-map-marker-al"></i>
                                                    
                                                    <p dangerouslySetInnerHTML={{ __html: props.settings.shop_address}}></p>
                                                </li>
                                                <li className="phone"><i className="icon anm anm-phone-l"></i>
                                                    <p>{props.settings.sale_contact}</p>
                                                </li>
                                                <li className="email"><i className="icon anm anm-envelope-l"></i>
                                                    <p>{props.settings.sale_email}</p>
                                                </li>
                                            </ul>

                                        </div>
                                        
                                        
                                        <div className="col-12 col-sm-12 col-md-2 col-lg-2 footer-links" dangerouslySetInnerHTML={{ __html: props.settings.section_block_1}}></div>
                                        <div className="col-12 col-sm-12 col-md-3 col-lg-3 footer-links" dangerouslySetInnerHTML={{ __html: props.settings.section_block_2}}></div>
                                        <div className="col-12 col-sm-12 col-md-3 col-lg-4">
                                            <div className="display-table">
                                                <div className="display-table-cell footer-newsletter">
                                                    <form action="#" method="post">
                                                        <label className="h4">Newsletter</label>
                                                        <p>Be the first to hear about new trending and offers and see how you've
                                                            helped.</p>
                                                        <div className="input-group">
                                                            <input type="email" className="input-group__field newsletter__input" name="EMAIL" value="" placeholder="Email address" required="" />
                                                            <span className="input-group__btn">
                                                                <button type="submit" className="btn newsletter__submit" name="commit" id="Subscribe"><span className="newsletter__submit-text--large">Subscribe</span></button>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div className="footer-social mt-3">

                                                <ul className="list--inline site-footer__social-icons social-icons">
                                                    <li><strong>Stay Connected:</strong></li>
                                                    <li><a className="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Facebook"><i className="icon icon-facebook"></i></a></li>
                                                    <li><a className="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Twitter"><i className="icon icon-twitter"></i> <span className="icon__fallback-text">Twitter</span></a></li>
                                                    <li><a className="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Pinterest"><i className="icon icon-pinterest"></i> <span className="icon__fallback-text">Pinterest</span></a></li>
                                                    <li><a className="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Instagram"><i className="icon icon-instagram"></i> <span className="icon__fallback-text">Instagram</span></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <hr></hr>
                                <div className="footer-bottom">
                                    <div className="row">
                                        
                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 order-1 order-md-0 order-lg-0 order-sm-1 copyright text-sm-center text-md-left text-lg-left">
                                            <span></span> <a href="templateshub.net">Developed by CodeByte Technologies</a></div>
                                       
                                        
                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 order-0 order-md-1 order-lg-1 order-sm-0 payment-icons text-right text-md-center">
                                            <img src={paylogo} alt="Payment" />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

          
                    </footer>
        </> 
  );

}


