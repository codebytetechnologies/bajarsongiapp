import React from 'react';

export default function Useraddress(props) {

  return(
            <>
                    <div id="address" className="address tab-pane">
											<h3 className="title2">Billing Address</h3>
											<a className="alert alert-success" role="alert">
												<strong>
													<i className="fa fa-plus-circle" aria-hidden="true"></i> ADD A NEW ADDRESS
												</strong>
											</a>
											<div className="card">
												<div className="card-body">
													<h2>
														Current Address
														<span className="badge editAddress">
															<a href="#" className="edit-i remove">
																<i className="anm anm-edit" aria-hidden="true"></i>
															</a>
														</span>
													</h2>
													<div>
														<strong>
															Baduria,Arsula, West Bengal 743401, Baduria, Baduria, West Bengal -
															743401
														</strong>
													</div>
												</div>
											</div>
										</div>
            </>
  );

}



                    
