import React from 'react';

export default function Userwishlist(props) {

  return(
            <>
                <div id="downloads" className="product-order tab-pane fade">
											<h3 className="title2">Wishlist (3)</h3>
											<div className="list-view-items grid--view-items">
												<div className="list-product list-view-item">
													<div className="list-view-item__image-column">
														<div className="list-view-item__image-wrapper">
															<a href="product-layout-2.html">
																<img
																	className="list-view-item__image blur-up ls-is-cached lazyloaded"
																	data-src="assets/images/product-images/product-image1.jpg"
																	src="assets/images/product-images/product-image1.jpg"
																	alt="image"
																	title="product"
																/>
															</a>
														</div>
													</div>
													<div className="list-view-item__title-column">
														<div className="h4 grid-view-item__title">
															<a href="product-layout-2.html">Camelia Reversible Jacket</a>
														</div>

														<p className="product-review">
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star-o"></i>
															<i className="font-13 fa fa-star-o"></i>
														</p>

														<p>
															Experience which can be used for various kinds of stores such as
															boutiques, bookstores, technology stores, jewelries and other types of
															web shops....
														</p>

														<p className="product-price grid-view-item__meta">
															<span className="old-price">$600.00</span>
															<span className="product-price__price product-price__sale">
																<span className="money">$500.00</span>
															</span>
														</p>

														<form className="variants" action="#">
															<button className="btn btn--small" type="button">
																Remove
															</button>
														</form>
													</div>
												</div>
												<div className="list-product list-view-item">
													<div className="list-view-item__image-column">
														<div className="list-view-item__image-wrapper">
															<a href="product-layout-2.html">
																<img
																	className="list-view-item__image blur-up ls-is-cached lazyloaded"
																	data-src="assets/images/product-images/product-image1.jpg"
																	src="assets/images/product-images/product-image1.jpg"
																	alt="image"
																	title="product"
																/>
															</a>
														</div>
													</div>
													<div className="list-view-item__title-column">
														<div className="h4 grid-view-item__title">
															<a href="product-layout-2.html">Camelia Reversible Jacket</a>
														</div>

														<p className="product-review">
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star-o"></i>
															<i className="font-13 fa fa-star-o"></i>
														</p>

														<p>
															Various kinds of stores such as boutiques, bookstores, technology
															stores, jewelries and other types of web shops....
														</p>

														<p className="product-price grid-view-item__meta">
															<span className="old-price">$600.00</span>
															<span className="product-price__price product-price__sale">
																<span className="money">$500.00</span>
															</span>
														</p>

														<form className="variants" action="#">
															<button className="btn btn--small" type="button">
																Remove
															</button>
														</form>
													</div>
												</div>
												<div className="list-product list-view-item">
													<div className="list-view-item__image-column">
														<div className="list-view-item__image-wrapper">
															<a href="product-layout-2.html">
																<img
																	className="list-view-item__image blur-up ls-is-cached lazyloaded"
																	data-src="assets/images/product-images/product-image1.jpg"
																	src="assets/images/product-images/product-image1.jpg"
																	alt="image"
																	title="product"
																/>
															</a>
														</div>
													</div>
													<div className="list-view-item__title-column">
														<div className="h4 grid-view-item__title">
															<a href="product-layout-2.html">Camelia Reversible Jacket</a>
														</div>

														<p className="product-review">
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star"></i>
															<i className="font-13 fa fa-star-o"></i>
															<i className="font-13 fa fa-star-o"></i>
														</p>

														<p>technology stores, jewelries and other types of web shops....</p>

														<p className="product-price grid-view-item__meta">
															<span className="old-price">$600.00</span>
															<span className="product-price__price product-price__sale">
																<span className="money">$500.00</span>
															</span>
														</p>

														<form className="variants" action="#">
															<button className="btn btn--small" type="button">
																Remove
															</button>
														</form>
													</div>
												</div>
											</div>
										</div>
            </>
  );

}
