import React, {useEffect , useState } from 'react';
import Productprice from '../Productprice/Productprice';
import * as Helper from '../../Helper';
import { useHistory } from "react-router-dom";
import {
    BrowserRouter as Router,
    Link,
  } from "react-router-dom";

export default function Product(props){


    console.log(props);

    let history = useHistory();
    //New
    const [Isnew, setIsnew] = useState(false);
    let today = new Date(),
    tdate =  today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
  
    //Sale
    const [Issale, setIssale] = useState(false);  

    const [logo, setlogo] = useState('');

    useEffect(() => {
        setlogo(Helper.SITEURL+'admin/picture/product/resize/450450/'+props.product.product_image[0]);
    }, [props.product.product_image[0]]);


    useEffect(() => {
        if(props.product.created_at !== undefined ){

            const date1 = new Date(props.product.created_at);
            const date2 = new Date(tdate);
            const diff = Helper.getDifferenceInDays(date1, date2);
    
            if (diff <= 20  ){
                setIsnew(true);
            }
        }
    },props.product.created_at)

    useEffect(() => {
        if(props.product.product_offer_percentage !== undefined ){
            if (props.product.product_offer_percentage > props.settings.new_product_range ){
                setIssale(true);
            }
        }
    },props.product.product_offer_percentage , props.settings.new_product_range)

    const pstyle = {
        'width': "100%",  
        'display': "inline-block"
    };
    let col = "col-6 col-sm-2 col-md-3 col-lg-"+props.col+" item";

  

    let url = '/product/'+props.product.product_slug;

    return(
                        <>

                            <div className={col} style= {pstyle}>
                                    
                                    <div className="product-image">
                                       
                                        <a href="javascript:void(0)" className="grid-view-item__link" tabindex="0">
                                            
                                            <img className="primary" data-src={logo} src={logo} alt="image" title="product" />
                                            <div className="product-labels rounded">
                                                
                                            {Issale === true ? (
                                                <span className="lbl on-sale">Sale</span>
                                            ) : (
                                                ''
                                            )}

                                            {Isnew === true ? (
                                                <span className="lbl pr-label1">new</span>
                                            ) : (
                                                ''
                                            )}
                                                
                                            </div>
                                          
                                        </a>
                                        
                                        <form className="variants add" action="javascript:void(0)" method="post">
                                            
                                            <button value={url} className="btn btn-addto-cart" tupe="button"  onClick={(e) => {e.preventDefault(); window.location.href=e.target.value;}} >Buy Now</button>
                                           
                                        </form>
                                        <div className="button-set">
                                            <a href="javascript:void(0)" title="Quick View" className="quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview" tabindex="0">
                                                <i className="icon anm anm-bag-l"></i>
                                            </a>
                                            <div className="wishlist-btn">
                                                <a className="wishlist add-to-wishlist" href="#" title="Add to Wishlist" tabindex="0">
                                                    <i className="icon anm anm-heart-l"></i>
                                                </a>
                                            </div>

                                        </div>

                                    </div>
                                    
                                    <div className="product-details text-center">

                                        <div className="product-name">
                                            <a href="product-layout-1.html" tabindex="0">{props.product.product_name}</a>
                                        </div>
                                        <div className="product-price">

                                            {props.product.product_retail_price !== undefined && props.product.product_retail_price > 0 ? (<Productprice price ={props.product.product_retail_price} sale ={props.product.product_offer_percentage} offerstatus={props.product.product_offer_status} type='type2' />) : ''}
                                            
                                        </div>
                                       
                                    </div>
                                   
                            </div>
 
                        </>
  );

}
