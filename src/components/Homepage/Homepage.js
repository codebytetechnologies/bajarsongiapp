import React, {useEffect , useState , Suspense} from 'react';

//Component
import Topbanner from '../Topbanner/Topbanner';
import Banner from '../Banner/Banner';
import Header from '../Header/Header';
import Productslider from '../Productslider/Productslider';
import Productpannel from '../Productpannel/Productpannel';
import Footer from '../Footer/Footer';
import Secondbanner from '../Secondbanner/Secondbanner';
import Productlist from '../Productlist/Productlist';
import Loader from '../Loader/Loader';
import * as Helper from '../../Helper';

import axios from 'axios';

export default function Homepage() {


  const [loaderState, setLoaderState] = useState(true);
  const [Sitesettings , setSitesettings] = useState([]);


  //API call
  let url = Helper.SITEURL+'admin/api/settings';
  const Apicall = () => {
    axios
        .get(url)
        .then((res) => {
            if (res.status === 200) {

              setLoaderState(false);
              if(res.data.data !== undefined) {
                setSitesettings(res.data.data);
              }
              
            }else{
              setLoaderState(true);
              setSitesettings([]);
            }
        })
        .catch((err) => {
            setLoaderState(true);
            setSitesettings([]);
        });
  };

  useEffect(() => {
      Apicall();
      
  }, []);


      //Best Products
      const [Bestproduct , setBestproduct] = useState([]);
      let bestproducturl = Helper.SITEURL+'admin/api/new-products';
      const Apibestproductcall = () => {
          axios
              .get(bestproducturl)
              .then((res) => {
  
                  if (res.status === 200) {

                      setLoaderState(false);
                      if(res.data.data !== undefined) {
                        setBestproduct(res.data.data);
                      }
                      
                  }else{
                      setLoaderState(true);
                      setBestproduct([]);
                  }
              })
              .catch((err) => {
                  setLoaderState(true);
                  setBestproduct([]);
              });
          };
      useEffect(() => {
        Apibestproductcall();
      }, [setBestproduct]);

      //New Product
      const [Newproduct , setNewproduct] = useState([]);
      let newproducturl = Helper.SITEURL+'admin/api/new-products';
      const Apinewproductcall = () => {
          axios
              .get(newproducturl)
              .then((res) => {
  
                  if (res.status === 200) {

                      if(res.data.data !== undefined) {
                        setNewproduct(res.data.data);
                      }
                      
                  }else{
                    setNewproduct([]);
                  }
              })
              .catch((err) => {
                  
                  setNewproduct([]);
              });
          };
      useEffect(() => {
        Apinewproductcall();
      }, [setNewproduct]);  

    return(
      <>
          {loaderState !== false  ? <Loader />  : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Topbanner settings = {Sitesettings} />  : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Header settings = {Sitesettings} /> : ''}
          {loaderState === false  ?  <Banner /> : ''}
          {loaderState === false  ?  <Productslider slidetitle='Weekly Bestseller' slidedescription='Our most popular products best sales on this weak' product ={Bestproduct} settings ={Sitesettings} /> : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Productpannel /> : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Secondbanner settings = {Sitesettings} /> : ''}
          {loaderState === false  ? <Productlist title='Weekly Bestseller' description='Our most popular products best sales on this weak' product ={Newproduct} settings = {Sitesettings} /> : ''}
          {loaderState === false && Sitesettings !== undefined  ? <Footer settings = {Sitesettings}/> : ''}
    
      </>
    );

}