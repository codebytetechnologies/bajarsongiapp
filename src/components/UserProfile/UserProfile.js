import React, { useEffect, useState, Suspense } from "react";
import axios from "axios";

// Components
import Topbanner from "../Topbanner/Topbanner";
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Loader from "../Loader/Loader";
import * as Helper from "../../Helper";

import Useraddress from  '../Useraddress/Useraddress';
import Userwishlist from  '../Userwishlist/Userwishlist';
import Userorders from  '../Userorders/Userorders';
import Useraccount from  '../Useraccount/Useraccount';


export default function UserProfile() {
	const [loaderState, setLoaderState] = useState(true);
	const [Sitesettings, setSitesettings] = useState([]);

	//API call
	let url = Helper.SITEURL + "admin/api/settings";
	const Apicall = () => {
		axios
			.get(url)
			.then((res) => {
				if (res.status === 200) {
					setLoaderState(false);
					if (res.data.data !== undefined) {
						setSitesettings(res.data.data);
					}
				} else {
					setLoaderState(true);
					setSitesettings([]);
				}
			})
			.catch((err) => {
				setLoaderState(true);
				setSitesettings([]);
			});
	};

	useEffect(() => {
		Apicall();
	}, []);
	return (
		<>	
			{loaderState !== false  ? <Loader />  : ''}
			{loaderState === false && Sitesettings !== undefined ? (<Topbanner settings={Sitesettings} />) : ("")}
			{loaderState === false && Sitesettings !== undefined  ? <Header settings = {Sitesettings} /> : ''}

			<div id="page-content" className="userContent">
				<div id="page-content">
					<div className="page section-header text-center" style={{marginBottom: '0px'}}>
						<div className="page-title">
							<div className="wrapper">
								<h1 className="page-width">User Profile</h1>
							</div>
						</div>
					</div>

					<div className="container">
						<div className="row profile">
							<div className="col-md-3">
								<div className="profile-sidebar">
									<div className="profile-userpic">
										<img
											src="https://gravatar.com/avatar/31b64e4876d603ce78e04102c67d6144?s=80&d=https://codepen.io/assets/avatars/user-avatar-80x80-bdcd44a3bfb9a5fd01eb8b86f9e033fa1a9897c3a15b33adfc2649a002dab1b6.png"
											className="img-responsive"
											alt=""
										/>
									</div>

									<div className="profile-usertitle">
										<div className="profile-usertitle-name">Jason Davis</div>
									</div>

									<div className="portlet light bordered mb-0">
										<ul className="nav flex-column dashboard-list" role="tablist">
											<li>
												<a className="nav-link active" data-toggle="tab" href="#dashboard">
													<i className="icon anm anm-dashboard"></i> Dashboard
												</a>
											</li>
											<li>
												<a className="nav-link" data-toggle="tab" href="#account-details">
													<i className="icon anm anm-user-circle-o"></i> Account details
												</a>
											</li>
											<li>
												<a className="nav-link" data-toggle="tab" href="#orders">
													<i className="fa fa-first-order"></i> Orders
												</a>
											</li>
											<li>
												<a className="nav-link" data-toggle="tab" href="#downloads">
													<i className="icon anm anm-heart"></i> Wishlist
												</a>
											</li>
											<li>
												<a className="nav-link" data-toggle="tab" href="#address">
													<i className="fa fa-address-book"></i> Addresses
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div className="col-md-9">
								<div className="profile-content">
									<div className="tab-content dashboard-content padding-30px-all md-padding-15px-all">
										<div id="dashboard" className="tab-pane fade active show">
											<h3>Dashboard</h3>
											<p>
												From your account dashboard. you can easily check &amp; view your
												<a className="text-decoration-underline" href="#">
													recent orders
												</a>
												, manage your
												<a className="text-decoration-underline" href="#">
													shipping and billing addresses
												</a>
												and
												<a className="text-decoration-underline" href="#">
													edit your password and account details.
												</a>
											</p>
										</div>
										<Useraccount/>
										<Userorders/>
										<Userwishlist/>
										<Useraddress/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{loaderState === false && Sitesettings !== undefined  ? <Footer settings = {Sitesettings}/> : ''}
		</>
	);
}
