import React from 'react';

export default function Useraccount(props) {
  let profileImage = "http://i.pravatar.cc/500?img=7";
  return(
            <>
                  <div id="account-details" className="tab-pane fade">
											<h3 className="title2">
												Account details
												<a href="#" className="edit-i remove">
													<i className="anm anm-edit" aria-hidden="true"></i>
												</a>
											</h3>
											<div className="account-login-form bg-light-gray padding-20px-all">
												<form>
													<fieldset>
														<div className="row">
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label for="input-firstname">
																	Profile Picture <span className="required-f">*</span>
																</label>
																<div className="avatar-upload">
																	<div className="avatar-edit">
																		<input
																			type="file"
																			id="imageUpload"
																			accept=".png, .jpg, .jpeg"
																		/>
																		<label for="imageUpload"></label>
																	</div>
																	<div className="avatar-preview">
																		<div
																			id="imagePreview"
																			style={{ backgroundImage: `url(${profileImage})` }}
																		></div>
																	</div>
																</div>
															</div>
														</div>

														<div className="row">
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label for="input-firstname">
																	First Name <span className="required-f">*</span>
																</label>
																<input
																	name="firstname"
																	value="John"
																	id="input-firstname"
																	className="form-control"
																	type="text"
																	disabled
																/>
															</div>
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label for="input-lastname">
																	Last Name <span className="required-f">*</span>
																</label>
																<input
																	name="lastname"
																	value="Smith"
																	id="input-lastname"
																	className="form-control"
																	type="text"
																/>
															</div>
														</div>
														<div className="row">
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label for="input-email">
																	E-Mail <span className="required-f">*</span>
																</label>
																<input
																	name="email"
																	value="John03@gmail.com"
																	id="input-email"
																	className="form-control"
																	type="email"
																/>
															</div>
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<a
																	className="passWordBtn"
																	href="#"
																	data-toggle="modal"
																	data-target="#content_quickview"
																>
																	Change Password
																</a>
																
															</div>
														</div>
														<div className="row">
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label for="input-telephone">
																	Mobile Number <span className="required-f">*</span>
																</label>
																<input
																	name="telephone"
																	value="+9178347876"
																	id="input-telephone"
																	className="form-control"
																	type="tel"
																/>
															</div>
															<div className="form-group col-md-6 col-lg-6 col-xl-6 required">
																<label>
																	Birthdate <span className="required-f">*</span>
																</label>
																<input
																	name="birthdate"
																	max="3000-12-31"
																	min="1000-01-01"
																	className="form-control"
																	type="date"
																/>
															</div>
														</div>
													</fieldset>

													<button type="submit" className="btn margin-15px-top btn-primary">
														Save
													</button>
												</form>
											</div>
										</div>
										
            </>
  );

}
