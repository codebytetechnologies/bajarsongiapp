import React from 'react';
import { FaHome } from "react-icons/fa";

export default function Breadcrumb (props) {
  return (
    <>
      <div className="bredcrumbWrap">
          <div className="container breadcrumbs">
              <a href="index.html" title="Back to the home page"><FaHome/></a><span
                  aria-hidden="true">/</span><span>Product Layout Style1</span>
          </div>
      </div>
    </>
  );
}
